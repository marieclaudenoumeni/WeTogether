/* Author : Noumen Youmbi Marie-Claude
  MAD SS20
  */

package com.example.wetogether

import android.content.Intent
import android.os.Bundle
import android.view.*
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import android.widget.Toast
import androidx.appcompat.app.ActionBar
//import android.widget.Toolbar
//import com.example.wetogether.ui.main.SectionsPagerAdapter
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import com.example.wetogether.TabPagerAdapter
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.system.exitProcess
import android.view.MenuItem
//import com.example.wetogether.chat.login
import com.example.wetogether.database.User
import com.example.wetogether.login.UsersListActivity

class MainActivity : AppCompatActivity() {

    private lateinit var mDrawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*val intent = Intent(this, HomeFragment::class.java)
        startActivity(intent)*/

        val tabPagerAdapter = TabPagerAdapter(supportFragmentManager, 5)


      //  val sectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = tabPagerAdapter

        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        // adda a nav drawer button
        val actionbar: ActionBar? = supportActionBar
        actionbar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_baseline_menu_24)
        }



       // mDrawerLayout = findViewById(R.id.drawer_layout)
   /*     val navigationView: NavigationView = findViewById(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener { menuItem ->
            // set item as selected to persist highlight
            menuItem.isChecked = true
            // close drawer when item is tapped
            mDrawerLayout.closeDrawers()

            // Handle navigation view item clicks here.

            when(menuItem?.itemId)
            {
                R.id.menuProfile -> {
                    var intent = Intent(this, UsersListActivity::class.java)
                    startActivity(intent)
                }
            }
            true

        }*/
        configureTabLayout()

        setupTabLayout(tabs)

        val user = User("fds", "password")
        println("=======================================")
        println(user.toString())
        // login(user)

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main,  menu)
        //inflater.inflate(R.menu.menu_contacts, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                Toast.makeText(applicationContext, "click on setting", Toast.LENGTH_LONG).show()
                true
            }
            R.id.action_search ->{
                Toast.makeText(applicationContext, "click on search", Toast.LENGTH_LONG).show()
                return true
            }
            R.id.action_exit ->{
                Toast.makeText(applicationContext, "click on exit", Toast.LENGTH_LONG).show()
                moveTaskToBack(true);
                exitProcess(-1)
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    fun setupTabLayout(tabLayout: TabLayout) {
        //tabLayout.tabMode = TabLayout.MODE_SCROLLABLE
        tabLayout.tabMode = TabLayout.MODE_FIXED
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        // tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        //tabLayout.setupWithViewPager(mViewpager)

        tabLayout.getTabAt(0)?.setIcon(R.drawable.ic_home_black_24dp)
        tabLayout.getTabAt(0)?.tabLabelVisibility = TabLayout.TAB_LABEL_VISIBILITY_UNLABELED

        tabLayout.getTabAt(1)?.setIcon(R.drawable.ic_fingerprint_black_24dp)
        tabLayout.getTabAt(1)?.tabLabelVisibility = TabLayout.TAB_LABEL_VISIBILITY_UNLABELED

        tabLayout.getTabAt(2)?.setIcon(R.drawable.ic_share_black_24dp)
        tabLayout.getTabAt(2)?.tabLabelVisibility = TabLayout.TAB_LABEL_VISIBILITY_UNLABELED

        tabLayout.getTabAt(3)?.setIcon(R.drawable.ic_chat_black_24dp)
        tabLayout.getTabAt(3)?.tabLabelVisibility = TabLayout.TAB_LABEL_VISIBILITY_UNLABELED

        tabLayout.getTabAt(4)?.setIcon(R.drawable.ic_person_black_24dp)
        tabLayout.getTabAt(4)?.tabLabelVisibility = TabLayout.TAB_LABEL_VISIBILITY_UNLABELED

    }

    private fun configureTabLayout() {

        tabs.addTab(tabs.newTab().setText("Tab 1 Item"))
        tabs.addTab(tabs.newTab().setText("Tab 2 Item"))
        tabs.addTab(tabs.newTab().setText("Tab 3 Item"))
        tabs.addTab(tabs.newTab().setText("Tab 4 Item"))
        tabs.addTab(tabs.newTab().setText("Tab 4 Item"))
        tabs.addTab(tabs.newTab().setText("Tab 5 Item"))

        val adapter = TabPagerAdapter(supportFragmentManager, 5)

        val pager: ViewPager = findViewById(R.id.view_pager)
        pager.adapter = adapter

        pager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
//                pager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }

        })
    }

}