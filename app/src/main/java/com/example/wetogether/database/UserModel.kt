package com.example.wetogether.database

data class UserModel (
    val uid: String = "",
    val name: String = "",
    var status: String = "",
    val photoUrl: String = ""
)

