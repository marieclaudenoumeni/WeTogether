package com.example.wetogether.fragment

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.example.wetogether.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.squareup.picasso.Picasso


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"



/**
 * A simple [Fragment] subclass.
 * Use the [ShareFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ShareFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_share, container, false)

        var view = inflater.inflate(R.layout.fragment_share, container, false)

        view?.findViewById<FloatingActionButton>(R.id.fab)?.setOnClickListener { view ->
            /*Snackbar.make(view, "picture upload", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()*/
            onBindImage()
        }

        return view
    }


    fun onBindImage() {

        val iv =  view?.findViewById<ImageView>(R.id.ImageView01) as ImageView
        val yourUrl = "https://cdn.pixabay.com/photo/2018/01/14/23/12/nature-3082832__340.jpg"
        //val yourUrl = "https://www.spiegel.de/politik/ausland/belarus-eu-staaten-erkennen-wahlergebnis-nicht-an-a-c5ec61b6-ea7b-4d8d-865f-444d1f261e8b"

        context?.let {
            Glide.with(it)
                .load(yourUrl)
                .override(700, 700)
                .into(iv)
        }

       // Picasso.get()
        // .load("https://www.spiegel.de/politik/ausland/belarus-eu-staaten-erkennen-wahlergebnis-nicht-an-a-c5ec61b6-ea7b-4d8d-865f-444d1f261e8b")
        // .into(iv)

       /* val url = "https://cdn.pixabay.com/photo/2018/01/14/23/12/nature-3082832__340.jpg"
        Picasso.with(this).load(url).into(iv)*/
    }

   /* fun loadImage(){
        val url = "../images/robot.png"
        val iv =  view?.findViewById<ImageView>(R.id.ImageView01) as ImageView
        //val iv = view?findViewById<ImageView>(R.id.imageView)
        Glide.with(this).load(url).into(iv)

        Glide.with(this).load(url).listener(object: RequestListener<Drawable> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {

            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {

            }

        })
            .into(iv)
    }*/
  /*  fun getView(position: Int, convertView: View?, parent: ViewGroup?) {
        var view: SquaredImageView? = convertView as SquaredImageView?
        if (view == null) {
            view = SquaredImageView(context)
        }
        val url: String = getItem(position)
        Picasso.get().load(url).into(view)
    }*/

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ShareFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ShareFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
