package com.example.wetogether.fragment

//import androidx.test.core.app.ApplicationProvider.getApplicationContext

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.wetogether.MainActivity
import com.example.wetogether.R
import com.example.wetogether.evaluations.AppEvaluationActivity
import com.example.wetogether.language.LocaleManager
import com.google.android.gms.flags.impl.SharedPreferencesFactory.getSharedPreferences
import kotlinx.android.synthetic.main.fragment_konto.*
import java.util.*
//import android.support.annotation.Nullable;


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


/**
 * A simple [Fragment] subclass.
 * Use the [KontoFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class KontoFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    val mLanguageCode  : String = "fr"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_konto, container, false)

        var view = inflater.inflate(R.layout.fragment_konto, container, false)


        var buttonlogout = view?.findViewById<Button>(R.id.logout)
        var buttonprofil = view?.findViewById<Button>(R.id.MeinProfil)
        var buttonhilfe = view?.findViewById<Button>(R.id.hilfe)
        var buttonevaluate= view?.findViewById<Button>(R.id.evaluate)

        buttonhilfe?.setOnClickListener(View.OnClickListener {
            Toast.makeText(context, getString(R.string.name_text), Toast.LENGTH_LONG).show()
        })

        buttonprofil?.setOnClickListener(View.OnClickListener {
            Toast.makeText(context, getString(R.string.name_account_created), Toast.LENGTH_LONG).show()
        })

        buttonevaluate?.setOnClickListener(View.OnClickListener {
            var intent = Intent(context, AppEvaluationActivity::class.java)
            startActivity(intent)
        })

        // when button is clicked, show the alert
        buttonlogout?.setOnClickListener(View.OnClickListener {

            val builder = AlertDialog.Builder(context)
            builder.setTitle(getString(R.string.textLogout4)).setMessage(getString(R.string.textLogout3))
            builder.setPositiveButton(getString(R.string.textLogout)) { dialog, id ->
                //val i = Intent(ApplicationProvider.getApplicationContext(), MainActivity::class.java)
                logout()
                val i = Intent(context, MainActivity::class.java)
                startActivity(i)
            }
            builder.setNegativeButton(getString(R.string.textLogout2)) { dialog, id -> dialog.cancel() }
            val alert11 = builder.create()
            alert11.show()
        })

        val currentLanguage:String = "en"
        val currentLang: String =""


        fun setLocale(localeName: String) {
            if (localeName != currentLanguage) {
                val myLocale = Locale(localeName)
                val res = resources
                val dm = res.displayMetrics
                val conf = res.configuration
                conf.locale = myLocale
                res.updateConfiguration(conf, dm)
                val intent = Intent(context, MainActivity::class.java)
                val refresh = intent
                refresh.putExtra(currentLang, localeName)
                startActivity(refresh)
            } else {
                Toast.makeText(context, "Language already selected!", Toast.LENGTH_SHORT).show()
            }
        }

        // access the items of the list
        val languages = resources.getStringArray(R.array.Languages)

        // access the spinner
        val spinner = view?.findViewById<Spinner>(R.id.spinner)

        // Create an ArrayAdapter
        // Apply the adapter to the spinner
        //spinner?.selectedItem != "Englisch"

        //if (spinner?.selectedItem != null) {
            //val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, languages)
            val adapter = context?.let { ArrayAdapter.createFromResource(it, R.array.Languages, android.R.layout.simple_spinner_item) }

            // Specify the layout to use when the list of choices appears
            adapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            spinner?.adapter = adapter
           // spinner.onItemSelectedListener = this

            spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                   Toast.makeText(context, getString(R.string.selected_item) + " " + "" + languages[position], Toast.LENGTH_SHORT).show()

                    /*if(spinner?.selectedItem== "English")
                        setNewLocale(context?.applicationContext, LocaleManager.ENGLISH)

                    if(spinner?.selectedItem== "German")
                        setNewLocale(context?.applicationContext, LocaleManager.GERMAN)

                    if(spinner?.selectedItem== "French")
                        setNewLocale(context?.applicationContext, LocaleManager.FRENCH)*/

                    when (position) {
                        0 -> {
                            setLocale("en")
                        }
                        1 -> {
                            setLocale("de")
                        }
                        2 -> {
                            setLocale("fr")
                        }
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }

        return view

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment KontoFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            KontoFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    fun logout(){
       val  PREFS_NAME:String  = "LoginPrefs"

        val myPrefs = activity?.getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
        val editor = myPrefs?.edit()
        editor?.remove("UserName")
        editor?.remove("PassWord")
        editor?.clear()
        editor?.commit()

        val intent = Intent(context, LoginFragment::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        /*intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)*/
    }

    fun doLogout() {
        val intent = Intent(context, LoginFragment::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        activity?.finish() // finish the current activity
    }

    fun switchLocal(context: Context, lcode: String, activity: Activity) {
        if (lcode.equals("", ignoreCase = true))
            return
        val resources: Resources = context.resources
        val locale = Locale(lcode)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        resources.updateConfiguration(config, resources.displayMetrics)
        //restart base activity
        activity.finish()
        activity.startActivity(activity.intent)
    }

    override fun onResume() {
        //buttonDate()
        changeLanguage()
        super.onResume()
    }
    fun changeLanguage(){
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val language = sharedPreferences.getString("language", "bak")
        Toast.makeText(context, language, Toast.LENGTH_SHORT).show()
        if(language=="English"){
            Toast.makeText(context, "English", Toast.LENGTH_SHORT).show()
            language("en")
        }else if(language=="french"){
            Toast.makeText(context, "french", Toast.LENGTH_SHORT).show()
            language("fr")
        }
    }

    fun language(language: String){
        val locale = Locale(language)
        Locale.setDefault(locale)
        val resources = resources
        val configuration = resources.configuration
        configuration.locale = locale
        resources.updateConfiguration(configuration, resources.displayMetrics)
    }

}
