package com.example.wetogether.fragment

import android.content.ContentValues.TAG
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide

import com.example.wetogether.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_chat.*
import android.content.Intent
import android.support.v4.media.session.MediaSessionCompat
import android.util.Log
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.example.wetogether.chat.*
import com.example.wetogether.notifications.Token
import com.google.firebase.iid.FirebaseInstanceId
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

import com.pusher.client.Pusher
import com.pusher.client.PusherOptions
import org.json.JSONObject

import androidx.appcompat.app.AppCompatActivity
import com.example.wetogether.MainActivity
import com.example.wetogether.chat.App.Companion.user
import com.example.wetogether.database.User
import com.example.wetogether.login.UsersListActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.fragment_chat.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

const val TAG = "ChatFragment"
val messagesList = arrayListOf<Message>()

/**
 * A simple [Fragment] subclass.
 * Use the [ChatFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ChatFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var adapter: MessageAdapter
    private lateinit var message:EditText
    private val pusherAppKey = "PUSHER_APP_KEY"
    private val pusherAppCluster = "PUSHER_APP_CLUSTER"
    val messagesList = arrayListOf<Message>()
    var user1: User? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat, container, false)

        /*val view = inflater.inflate(R.layout.fragment_chat, container, false)

        return view*/
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        messageList.layoutManager = LinearLayoutManager(context)
        adapter = context?.let { MessageAdapter(it) }!!
        messageList.adapter = adapter

        message =  view?.findViewById<EditText>(R.id.txtMessage)

        //adapter?.notifyItemInserted(messagesList.size)
        // messageList.scrollToPosition(messagesList.size-1)

        val intent = Intent(context, MainActivity::class.java)
        if (intent.extras != null) {
            user1 = User(
                email = intent.getStringExtra("email"),
                password = intent.getStringExtra("password")
            )
            sendMessage(message.text.toString())

        }
        else {
            Toast.makeText(context, "Message should not be empty", Toast.LENGTH_SHORT).show()
            //activity?.finish()
        }

      /*  val email:String = ""
        val password: String =""
        val user =  User(email, password)*/

        btnSend?.setOnClickListener(View.OnClickListener {
                if (message.text.isNotEmpty()) {
                    val message = Message(
                        //user
                        //App.user,  //user send message
                        message.text.toString(),
                        Calendar.getInstance().timeInMillis
                    )
                    val call = ChatService.create().postMessage(message)

                    call.enqueue(object : Callback<Void> {
                        override fun onResponse(call: Call<Void>, response: Response<Void>) {
                            resetInput()
                            if (!response.isSuccessful) {
                                Log.e(TAG, response.code().toString());
                                Toast.makeText(context, "Response was not successful", Toast.LENGTH_SHORT).show()
                            }
                        }

                        override fun onFailure(call: Call<Void>, t: Throwable) {
                            resetInput()
                            Log.e(TAG, t.toString())
                            Toast.makeText(context, "Error when calling the service", Toast.LENGTH_SHORT).show()
                        }
                    })
                } else {
                    Toast.makeText(context, "Message should not be empty", Toast.LENGTH_SHORT).show()
                }

                loadDummyMessages()
            //
        })

        setupPusher()

        view?.findViewById<FloatingActionButton>(R.id.fab_user)?.setOnClickListener { view ->

           var intent = Intent(context, UsersListActivity::class.java)
            startActivity(intent)

        }
    }

    private fun resetInput() {
        // Clean text box
        message.text.clear()

        // Hide keyboard
        val inputManager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(activity?.currentFocus?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    private fun setupPusher() {
        val options = PusherOptions()
        options.setCluster(pusherAppCluster)

        val pusher = Pusher(pusherAppKey, options)
        val channel = pusher.subscribe("chat")

        channel.bind("new_message") { channelName, eventName, data ->
            val jsonObject = JSONObject(data)

            val message = Message(
                jsonObject["message"].toString(),
                jsonObject["time"].toString().toLong()
            )

            activity?.runOnUiThread {
                adapter.addMessage(message)
                // scroll the RecyclerView to the last added element
                messageList.scrollToPosition(adapter.itemCount - 1);
            }

        }

        pusher.connect()
    }

    private fun sendMessage(message: String) {
        if (!message.isEmpty()) {
            user1?.let {

                messagesList.add(Message(message.toString(),  Calendar.getInstance().timeInMillis))
            }
        }
    }

    /*fun Fragment?.runOnUiThread(action: () -> Unit) {
        this ?: return
        if (!isAdded) return // Fragment not attached to an Activity
        activity?.runOnUiThread(action)
    }*/

    fun loadDummyMessages() {
        messagesList.add(Message( "Hi", 10))
        messagesList.add(Message("How are you?", 10))
        messagesList.add(Message("I´m fine", 10))
        messagesList.add(Message("and you?", 10))

        Toast.makeText(context, "Message should not be empty", Toast.LENGTH_SHORT).show()
    }

    //compare both of users

    /* override fun getItemViewType(position: Int): Int {

        return if(App.user == message.user) {
            VIEW_TYPE_MY_MESSAGE
        }
        else {
            VIEW_TYPE_OTHER_MESSAGE
        }
    }
*/

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ChatFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ChatFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}




