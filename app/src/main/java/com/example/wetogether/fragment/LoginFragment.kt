package com.example.wetogether.fragment

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.CompoundButton
import androidx.fragment.app.Fragment
import com.example.wetogether.MainActivity
import com.example.wetogether.R
import com.example.wetogether.chat.Login
//import com.example.wetogether.chat.httpLogin

import com.example.wetogether.database.DatabaseHelper
import com.example.wetogether.database.InputValidation
import com.example.wetogether.database.User
import com.example.wetogether.login.SignupActivity
import com.example.wetogether.login.UsersListActivity
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.coroutines.*
import org.json.JSONObject


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private lateinit var databaseHelper: DatabaseHelper
private lateinit var inputValidation: InputValidation

/**
 * A simple [Fragment] subclass.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoginFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    var _emailText: EditText? = null
    var _passwordText: EditText? = null
    var _loginButton: Button? = null
    var _signupLink: TextView? = null
    lateinit var checkBox:CheckBox


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       // return inflater.inflate(R.layout.fragment_login, container, false)
        var view = inflater.inflate(R.layout.fragment_login, container, false)

        _loginButton = view?.findViewById<Button>(R.id.login)
        _signupLink  = view?.findViewById<TextView>(R.id.message)
        _passwordText = view?.findViewById<EditText>(R.id.password)
        _emailText = view?.findViewById<EditText>(R.id.username)

        databaseHelper = context?.let { DatabaseHelper(it) }!!
        inputValidation = InputValidation(requireContext())

        val onClickListener = _loginButton?.setOnClickListener(View.OnClickListener {
            login()
            //this.login()
            /* val intent = Intent(this, SettingsActivity::class.java)
             startActivity(intent)*/

            //check user
            verifyFromSQLite()
        })

        _signupLink?.setOnClickListener {
            // Start the Signup activity
            val intent = Intent(context, SignupActivity::class.java)
            startActivityForResult(intent, REQUEST_SIGNUP)
            activity?.finish()
            //   overridePendingTransition(com.WeTogether.R.anim.push_left_in, com.WeTogether.R.anim.push_left_out)
        }

        checkBox = view?.findViewById<CheckBox>(R.id.checkbox)!!
        checkBox.setOnClickListener {
            val isChecked: Boolean = true
            //var isChecked = true
            if (!isChecked) {
                // show password
                password.transformationMethod = HideReturnsTransformationMethod.getInstance()
            } else {
                // hide password
                password.transformationMethod = PasswordTransformationMethod.getInstance()
            }
        }

   /*     checkbox.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, isChecked ->
            if (!isChecked) {
                // show password
                password.transformationMethod = HideReturnsTransformationMethod.getInstance()
            } else {
                // hide password
                password.transformationMethod = PasswordTransformationMethod.getInstance()
            }
        })*/

        val email = _emailText!!.text.toString()
        val password = _passwordText!!.text.toString()

        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment LoginFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            LoginFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }

        private val TAG = "LoginActivity"
        private val REQUEST_SIGNUP = 0
    }

// suspend fun login

  fun login() {
        Log.d(TAG, "Login")

        if (!validate()) {
            onLoginFailed()
            return
        }

        _loginButton!!.isEnabled = false

        val progressDialog = ProgressDialog(context, com.example.wetogether.R.style.AppTheme_Dark_Dialog)
        progressDialog.isIndeterminate = true
        progressDialog.setMessage("Login...")
        progressDialog.show()

        val email = _emailText!!.text.toString()
        val password = _passwordText!!.text.toString()

        val user =  User(email, password)
        //httpLogin(user)
        //Login(user)
        // TODO: Implement your own authentication logic here.

        android.os.Handler().postDelayed(
            {
                // On complete call either onLoginSuccess or onLoginFailed
                onLoginSuccess()
                // onLoginFailed();
                progressDialog.dismiss()
            }, 3000)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == Activity.RESULT_OK) {

                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                activity?.finish()
            }
        }
    }

    fun onBackPressed() {
        // Disable going back to the MainActivity
        activity?.moveTaskToBack((true))
        //getActivity().moveTaskToBack(true);
    }

    fun onLoginSuccess() {
        _loginButton!!.isEnabled = true
        activity?.finish()
        //startActivity(Intent(this, MainActivity::class.java)

        val intent = Intent(context, MainActivity::class.java)
        startActivity(intent)
    }

    fun onLoginFailed() {
        Toast.makeText(context, "Login failed", Toast.LENGTH_LONG).show()

        _loginButton!!.isEnabled = true
    }

    fun validate(): Boolean {
        var valid = true

        val email = _emailText!!.text.toString()
        val password = _passwordText!!.text.toString()

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText!!.error = "enter a valid email address"
            valid = false
        } else {
            _emailText!!.error = null
        }

        if (password.isEmpty() || password.length < 4 || password.length > 10) {
            _passwordText!!.error = "between 4 and 10 alphanumeric characters"
            valid = false
        } else {
            _passwordText!!.error = null
        }
        return valid
    }



    /**
     * This method is to validate the input text fields and verify login credentials from SQLite
     */
    fun verifyFromSQLite() {
        if (databaseHelper!!.checkUser(_emailText!!.text.toString().trim { it <= ' ' }, _passwordText!!.text.toString().trim { it <= ' ' })) {

            val accountsIntent = Intent(activity, UsersListActivity::class.java)
            accountsIntent.putExtra("EMAIL", _emailText!!.text.toString().trim { it <= ' ' })
            startActivity(accountsIntent)
        }
    }

    /* val obj = JSONObject(response)

        //getting the user from the response
        val userJson = obj.getJSONObject("user")

        //creating a new user object
        val user = User(
            userJson.getInt("id"),
            userJson.getString("username"),
            userJson.getString("email"),
            userJson.getString("password")
        )
*/



}



